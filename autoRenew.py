#!/usr/bin/env python3.8
# encoding: utf-8
import config
import sysbus
import subprocess
from freepybox import Freepybox

def freeboxNat(action):
    fbx = Freepybox(api_version='v4')
    fbx.open('mafreebox.freebox.fr',443)
    fbx._access.put('fw/redir/3',{"enabled": action})
    fbx.close()
    
def liveboxNat(action):
    #Active ou Descative la règle NAT sur le livebox pour permettre à letsencrypt de joindre le NAS
   
    subprocess.run(["python3", "/opt/bin/letsEncryptAutoRenew/sysbus.py", "sysbus.Firewall:setPortForwarding", "id=HTTP NAS", "description=HTTP%20Nas", "origin=webui", "sourceInterface=data", "internalPort=80", "externalPort=80", "destinationIPAddress=192.168.1.60", "protocol=6,17", "enable="+str(action)])
    
def natEnable(action):
    if config.box == "freebox":
        freeboxNat(action)
    else:
        if config.box == "livebox":
            liveboxNat(action)
        else:
            print("Error in config file, enter either 'freebox' or 'livebox'")

   
def main(): 

    #Activate Nat rule
    natEnable(True)

    #Renew certificate
    subprocess.run(["/usr/syno/sbin/syno-letsencrypt", "renew-all", "-v"]) #-vv for debug

    #Disable Nat rule
    natEnable(False)

    #Restart Nginx & OpenVpn
    subprocess.run(["synoservicectl", "--restart", "nginx"])
    subprocess.run(["/var/packages/VPNCenter/target/scripts/openvpn.sh", "restart"])

if __name__ == "__main__":
    main()
